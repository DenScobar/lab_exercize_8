'use strict';

let numberOfFilms;

// Первый способ
do {
    numberOfFilms = prompt('Сколько фильмов вы уже посмотрели?');
}
while (numberOfFilms == "" || numberOfFilms == null || numberOfFilms.length > 50)

// Второй спопсоб
/* while (true) {
    if (numberOfFilms != "" && numberOfFilms != null && numberOfFilms.length < 50) {
        break;
    }
    numberOfFilms = prompt('Сколько фильмов вы уже посмотрели?');
} */

// Третий спопсоб
/* while (true) {
    numberOfFilms = prompt('Сколько фильмов вы уже посмотрели?');
    if (numberOfFilms == "" || numberOfFilms == null || numberOfFilms.length > 50) {
        continue;
    }
    break;
} */


let personalMovieDB = {
        count: numberOfFilms,
        movies: {},
        actors: {},
        genres: [],
        privat: false,
    }
    // personalMovieDB.movies[prompt('Один из последних просмотренных фильмов?')] = prompt('На сколько оцените его?');

if (+personalMovieDB.count < 10) {
    console.log("Просмотрено довольно мало фильмов");
} else if (+personalMovieDB.count < 30 && +personalMovieDB.count >= 10) {
    console.log("Вы классический зритель");
} else if (+personalMovieDB.count >= 30) {
    console.log("Вы киноман");
} else {
    console.log("Произошла ошибка");
}